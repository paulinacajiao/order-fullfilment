# Order Fulfillment API
This is the list of connectors used on each release
## Release 4.2
Asset | Version
------|---------
Mule Runtime | 4.1.1
Mule Maven Plugin | 3.1.1
MUnit  |  2.1.0
APIKit  |  1.1.1
Mule Http Connector  |  1.2.0
Mule Sockets Connector  |  1.1.1  
Mule MQ Connector  |  2.0.0
Mule Customer API Connector  |  1.0.0
Mule Order API Connector  |  1.0.0
Mule Notification API Connector  |  1.0.1
